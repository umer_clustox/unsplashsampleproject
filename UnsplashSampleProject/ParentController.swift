//
//  ParentController.swift
//  UnsplashSampleProject
//
//  Created by Nate Wilson on 13/09/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ParentController: UIViewController, NVActivityIndicatorViewable {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showAnimation() {
        let size = CGSize(width: 50, height:50)
        startAnimating(size, message: "", type: NVActivityIndicatorType.lineScale)
    }
    
    func hideAnimation() {
        DispatchQueue.main.async {
            self.stopAnimating()
        }
    }
}

extension ParentController: AnimationDelegate {
    /// Callback function for showing animation
    func didStartAnimating() {
        self.showAnimation()
    }
    
    /// Callback function for hiding animation
    func didStopAnimating() {
        self.hideAnimation()
    }
}

public protocol AnimationDelegate: class {
    /// Callback function for showing animation
    func didStartAnimating()
    
    /// Callback function for hiding animation
    func didStopAnimating()
}
