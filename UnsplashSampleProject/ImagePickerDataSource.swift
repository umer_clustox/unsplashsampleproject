//
//  ImagePickerDataSource.swift
//  UnsplashSampleProject
//
//  Created by Nate Wilson on 13/09/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

final class ImagePickerDataSource: NSObject {
    
    // Collection View
    weak var collectionView: UICollectionView?
    
    var animationDelegate: AnimationDelegate?
    
    ///collectionViewTappedDelegate
    var delegate: ImagePickerViewControllerDelegate?
    
    let reuseableIdentifier = "image_cell"
    
    var imagesUrl = [String]()
    
    let service = SearchImageService()
    
    /// Initialization function
    ///
    /// - Parameter collectionView: The CollectionView
    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        super.init()
        
        configureCollectionView()
    }
    
    /// Function for configuring up the collection view
    func configureCollectionView() {
        self.collectionView?.dataSource = self
        self.collectionView?.delegate = self
        
    }
    
    func fetchData(query: String) {
        
        self.animationDelegate?.didStartAnimating()
        
        self.service.searchImages(query: query, completion: {
            data, error in
            
            self.animationDelegate?.didStopAnimating()
            guard let urls = data else {
                return
            }
            
            self.imagesUrl = urls
            self.collectionView?.reloadData()
            
        })
    }
}

extension ImagePickerDataSource: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return self.imagesUrl.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.reuseableIdentifier , for: indexPath) as! ImageCell
        
        KingfisherManager.shared.downloader.downloadTimeout = 50
        
        if let pictureUrl = URL(string: self.imagesUrl[indexPath.row]) {
            cell.image.kf.indicatorType = .activity
            cell.image.kf.setImage(with: pictureUrl, placeholder: UIImage(named: "default_image"))
        }else{
            cell.image.image = UIImage(named: "default_image")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let cell = collectionView.cellForItem(at: indexPath) as? ImageCell else {
            return
        }
        
        let imageUrl = self.imagesUrl[indexPath.row]
        self.delegate?.imageViewTapped(url: imageUrl)
    }
    
}

extension ImagePickerDataSource: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let minSpace: CGFloat = 15
        let width = (collectionView.frame.size.width - minSpace)/2.2
        return CGSize(width: width,height: width*0.7)
    }
    
}

public protocol ImagePickerViewControllerDelegate: class {
    
    func imageViewTapped(url: String)
}
