//
//  SearchImageService.swift
//  UnsplashSampleProject
//
//  Created by Nate Wilson on 13/09/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import Alamofire

public typealias JSON = Any
public typealias JSONDictionary = [String: JSON]
public typealias JSONArray = [JSON]

public class SearchImageService {
    
    
    /// Handler for search image request
    let networkHandler : NetworkHandler
    
    
    public init() {
        self.networkHandler = NetworkHandler()
    }
    
    /// Requests handler to search Images form unsplash.com
    ///
    /// - Parameters:
    ///   - query: content to search
    ///   - completion: completion block
    public func searchImages(query: String, completion: DataHandler? = nil) {
        
        var imagesUrl = [String]()
        
        let url = "https://api.unsplash.com/search/photos?query="
        
        //Setting query string with url
        
        let urlString = url.appending(query.replacingOccurrences(of: " ", with: ",")).appending("&per_page=50")
        
        
        let queryUrl = URL(string: urlString)
        
        let headers: HTTPHeaders = ["Authorization": "Client-ID 8f99552324bc786aac655d4e5177ac54fb866a276ec804c0d2efd9de55607ba2",
                                    "Accept-Version": "v1",
                                    "Content-Type":"application/json",
                                    "Accept":"application/json"]
        
        self.networkHandler.request(headers: headers, url: queryUrl!)
        { response, error in
            
            guard let responseData = response else {
                completion?(nil, error)
                return
            }
            
            //Checking if error exists or not
            //Getting search results array
            guard error == nil,
                let searchResults = responseData.result.value as? JSONDictionary,
                let results = searchResults["results"] as? JSONArray else {
                    completion?(nil, error)
                    return
            }
            
            //Getting urls object from array
            //Extracting link of regular image from urls object
            for data in results {
                guard let urlData = data as? JSONDictionary,
                    let urls = urlData["urls"] as? JSONDictionary else {
                        completion?(nil, error)
                        return
                }
                if let url = urls["regular"] as? String {
                    imagesUrl.append(url)
                }
            }
            
            completion?(imagesUrl, nil)
        }
    }
}

public typealias DataHandler = (_ data: [String]?, _ error: Error?) -> ()

