//
//  ViewController.swift
//  UnsplashSampleProject
//
//  Created by Nate Wilson on 13/09/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import UIKit
import Kingfisher

class ViewController: ParentController {

    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    var dataSourceController: ImagePickerDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchBar.delegate = self
        self.dataSourceController = ImagePickerDataSource(collectionView: self.imageCollectionView)
        self.dataSourceController?.delegate = self
        self.dataSourceController?.animationDelegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        KingfisherManager.shared.cache.clearMemoryCache()
        KingfisherManager.shared.cache.clearDiskCache()
        KingfisherManager.shared.cache.cleanExpiredDiskCache()
    }
}
extension ViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        guard let searchQuery = searchBar.text else {return}
        
        self.dataSourceController?.fetchData(query: searchQuery)
        
    }
}

extension ViewController: ImagePickerViewControllerDelegate {
    
    func imageViewTapped(url: String) {
        
        print(url)
    }
}

