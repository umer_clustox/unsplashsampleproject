//
//  NetworkHandler.swift
//  UnsplashSampleProject
//
//  Created by Nate Wilson on 13/09/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import Alamofire

public struct NetworkHandler {
    
    var commonHeaders: HTTPHeaders = ["Content-Type":"application/json",
                                      "Accept":"application/json"]
    
    public typealias ResponseHandler = (_ response: DataResponse<Any>?, _ error: Error?) -> ()
    
    public func request(headers: [String: String]? = nil, params: [String: Any]? = nil , url: URL, completion: ResponseHandler? = nil) {
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success:
                completion?(response, nil)
            case .failure(let error):
                completion?(response, error)
            }
        }
        
    }
}
