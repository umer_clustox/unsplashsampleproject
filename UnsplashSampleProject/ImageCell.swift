//
//  ImageCell.swift
//  UnsplashSampleProject
//
//  Created by Nate Wilson on 13/09/2017.
//  Copyright © 2017 Mac. All rights reserved.
//

import Foundation
import UIKit

class ImageCell: UICollectionViewCell {
    
    
    @IBOutlet weak var image: UIImageView!

}
